This model artifact is committed to the repo for the user's convenience.
For real models, specially those with heavy model artifacts, model artifacts
should NOT be committed to the repo.
